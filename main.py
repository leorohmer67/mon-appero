import sys
import reel.real as real

if __name__ == "__main__":
    argv = sys.argv[1:]

    # <nom de la ville> <nom du pays> <nombre de vehicules> [prix de l'essence]
    if len(argv) < 3:
        print('Utilisation:\n\tpython main.py <nom de la ville> <nom du pays> <nombre de vehicules> [prix de l\'essence] [parcourir les trottoirs]')
        exit(1)

    cityName = argv[0] + ', ' + argv[1]
    try:
        vehiculeNumber = int(argv[2])
    except:
        print("\"" + argv[2] + "\" n'est pas un nombre")
        exit(2)

    trottoir = False
    essenceCost = 2.36
    if len(argv) > 3:
        try:
            if argv[3] != '-t':
                essenceCost = int(argv[3])
            else:
                trottoir = True

            if len(argv) > 4:
                if argv[4] == '-t':
                    trottoir = True
                else:
                    essenceCost = int(argv[4])
        except:
            print("\"" + argv[3] + "\" n'est pas un nombre")
            exit(2)
    else:
        essenceCost = 2.36


    try:
        real.launch(cityName, vehiculeNumber, essenceCost, trottoir)
    except ValueError:
        print('Erreur d\'execution')
        print(ValueError)
        exit(3)