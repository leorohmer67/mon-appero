import math
from reel import helper
from reel.find_shortest_path import findShortestPath

def matrix_graph_undirected(nodes, edges):
    nb = len(nodes)
    mat = [([0] * nb) for _ in range(nb)]
    for edge in edges:
        mat[nodes.index(edge[0])][nodes.index(edge[1])] += 1
        mat[nodes.index(edge[1])][nodes.index(edge[0])] += 1

    for i in range(nb):
        for j in range(nb):
            if mat[i][j] != 0 and mat[i][j] != mat[j][i]:
                mat[i][j] = 1
                mat[j][i] = 1

    return mat


def isEulerian(adjList):
    for adj in adjList:
        if len(adj) & 1:
            return False
    return True


def find_min(n, dist):
    mini = math.inf
    sommet = -1
    for s in n:
        if dist[n.index(s)] < mini:
            mini = dist[n.index(s)]
            sommet = s

    return sommet


def find_closest(list_adj, nodes, src, impair):
    queue = [src]
    visited = [False] * len(nodes)
    while queue:
        sommet = queue.pop(0)
        visited[nodes.index(sommet)] = True
        for voisin in list_adj[nodes.index(sommet)]:
            if not visited[nodes.index(voisin)] and voisin not in queue:
                queue.append(voisin)
            if voisin in impair:
                return voisin
    return impair[0]


def kwan_undirected(nodes, edges):
    nb = len(nodes)

    adjlist = helper.makeAdjListFromDirectedToUndirected(nodes, edges)

    # Transforme la matrice en edges
    new_edges = helper.makeEdgesFromAdjList(nodes, adjlist)

    # Calcul du nombre de sommets impairs, les mets dans une liste
    impair = helper.oddNodes(nodes, adjlist)

    if len(impair) % 2 == 1:
        print("Nombre impair de sommets impairs, dommage...")
        exit(2)

    # Applique l'algorithme de Dijkstra pour trouver le chemin le plus optimale pour relier les deux sommets
    while len(impair) != 0:
        src = impair[0]
        dest = find_closest(adjlist, nodes, src, impair[1:])
        path = findShortestPath(nodes, adjlist, src, dest, nb)

        impair.remove(src)
        impair.remove(dest)
        for i in range(len(path) - 1):
            new_edges.append((path[i], path[i + 1]))

    # Calcul du nombre de sommets impairs, les mets dans une liste
    impair = helper.oddNodes(nodes, helper.makeAdjList(nodes, new_edges, False))

    return new_edges


def make_undirected_eulerian(nodes, edges):
    adjList = helper.makeAdjListFromDirectedToUndirected(nodes, edges)
    new_edges = edges[:]

    euler = isEulerian(adjList)
    print("Le graph non orienté est eulérien" if euler else "Le graph non orienté n'est pas eulérien")

    if not euler:
        print("Formatage en cours...")
        print("Nombres d'edges initial :", len(edges))
        new_edges = kwan_undirected(nodes, edges)

        print("Nombres d'edges final :", len(new_edges))
        adjList = helper.makeAdjList(nodes, new_edges, False)
        euler = isEulerian(adjList)
        print("Le graph non orienté est finalement eulérien" if euler else "Le graph n'est toujours pas eulérien")

    return new_edges
