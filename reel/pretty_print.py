def pretty_print(recap_tab, vehicles_tab, vehicleNumber, totalLengthRoad, totalCost, isDrive):
    print('{0:^100}'.format("RECAP"))
    print("Total véhicule: " + str(vehicleNumber))
    print("Longeur totale: " + str(totalLengthRoad) + "km")
    print("Total coût théorique: " + str(totalCost) + " $CAD")
    print()

    form = "{0:^20}{1:^20}{2:^20}{3:^25}{4:^25}"
    for val in recap_tab:
        print(form.format(*val))

    # création et écriture dans les différents fichiers
    print()
    print("Création des fichiers pour chaque véhicule...")

    if isDrive:
        name = "Vehicule"
    else:
        name = "VehiculeWalk"
    i = 1
    for val in vehicles_tab:
        with open("VehiculeAlone.txt", "w+") if vehicleNumber == 1 else open(name + str(i) + ".txt", "w+") as file:
            file.write("{:^12} -> {:^12}: {:^10}, {}\n\n".format("Départ", "Fin", "Longueur", "Nom"))

            for road in val:
                line = "{0:12} -> {1:12}: {2:8}km, {3}\n".format(road[1], road[2], road[3], road[4])
                file.write(line)

        i += 1

    print("Les fichiers ont été créés. Ils sont sous le format 'VehiculeX.txt'.")

def divide(L, n):
    R = dict()
    index = 0
    while L:
        length = len(L) // (n - index)
        t = []
        for i in range(length):
            t.append(L.pop(0))
        R[index] = t
        index += 1

    return R

def main_print(circuit:list, graph, vehiclesNumber:int = 1, EssenceCost = 2.36, isDrive = True):
    recap_vehicles = [["Véhicule", "Position de départ", "Position d'arrivée", "Longueur totale (km)", "Coût théorique ($CAD)"]]
    vehicles_detail = []
    truckNumber = 1
    globalLength = 0
    totalLength = 0
    totalCost = 0

    circuit_each_vehicle = divide(circuit, vehiclesNumber)

    # iterate the number of vehicle we got
    for vehicle in circuit_each_vehicle:
        # Create the recap for the current vehicle
        initialNode = circuit_each_vehicle[vehicle][0][0] # first node
        finalNode = circuit_each_vehicle[vehicle][-1][1] # change to the final node

        roadDetail = []
        for road in circuit_each_vehicle[vehicle]:
            try:
                roadName = graph.edges[(road[0], road[1], 0)]["name"] if "name" in graph.edges[
                    (road[0], road[1], 0)] else "Nan"
                roadLength = graph.edges[(road[0], road[1], 0)]["length"] if "length" in graph.edges[
                    (road[0], road[1], 0)] else "Nan"
            except:
                try:
                    roadName = graph.edges[(road[1], road[0], 0)]["name"] if "name" in graph.edges[
                        (road[1], road[0], 0)] else "Nan"
                    roadLength = graph.edges[(road[1], road[0], 0)]["length"] if "length" in graph.edges[
                        (road[1], road[0], 0)] else "Nan"
                except:
                    roadName = "Nan"
                    roadLength = 42

            roadLength = round(roadLength / 1000, 2)
            roadDetail.append((truckNumber, road[0], road[1], roadLength, roadName))
            totalLength += roadLength

        # consommation of 2.5L/km
        cost = totalLength * 2.5 * EssenceCost # essence cost
        if isDrive:
            cost += (totalLength / 20) * 15.2 # vehicle worker
        else:
            cost += (totalLength / 10) * 15.2 # pavement worker

        totalCost += cost

        current_vehicle_recap = [truckNumber, initialNode, finalNode, round(totalLength, 2), round(cost, 2)]
        recap_vehicles.append(current_vehicle_recap)

        vehicles_detail.append(roadDetail)

        # restore the values
        truckNumber += 1
        globalLength += totalLength
        totalLength = 0

    # call the pretty print with all values
    pretty_print(recap_vehicles, vehicles_detail, vehiclesNumber, round(globalLength, 2), round(totalCost, 2), isDrive)


def drone_print(circuit:list, graph):
    with open("drone.txt", "w+") as file:
        file.write("{:^12} -> {:^12}: {:^10}, {}\n\n".format("Départ", "Fin", "Longueur", "Nom"))
        for val in circuit:
            try:
                road_name = graph.edges[(val[0], val[1], 0)]["name"] if "name" in graph.edges[(val[0], val[1], 0)]\
                    else "Nan"
                roadLength = graph.edges[(val[0], val[1], 0)]["length"] if "length" in graph.edges[(val[0], val[1], 0)]\
                    else "Nan"
            except:
                road_name = graph.edges[(val[1], val[0], 0)]["name"] if "name" in graph.edges[(val[1], val[0], 0)]\
                    else "Nan"
                roadLength = graph.edges[(val[1], val[0], 0)]["length"] if "length" in graph.edges[(val[1], val[0], 0)]\
                    else "Nan"

            line = "{0:12} -> {1:12}: {2:^8}km, {3}\n".format(str(val[0]), str(val[1]), str(round(roadLength / 1000, 2)), road_name)
            file.write(line)