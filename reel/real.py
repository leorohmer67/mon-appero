from reel import helper
import osmnx as ox

from reel.make_undirected_eulerian import make_undirected_eulerian
from reel.make_directed_eulerian import make_directed_eulerian
from reel.find_eulerian_circuit import findEulerianCircuit
import reel.pretty_print as pretty_print


def launch(name_place, nbVehicules, essenceCost, walk):
    # Vous pouvez modifier ici la ville à utiliser.
    g = ox.graph_from_place(name_place, network_type='drive')

    nodes = list(g.nodes)
    edges = list(g.edges)

    # =============================================== DRONE ============================================================

    # Vérifie si le graph en non orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    drone_edges = make_undirected_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, drone_edges, False)
    drone_path = findEulerianCircuit(nodes, nodes[0], adjList)

    print("\n========================== DRONE ==========================")
    pretty_print.drone_print(drone_path, g)
    print("Création d'un fichier 'drone.txt' avec le chemin du drone.")
    drone_path = None
    drone_edges = None

    # =============================================== CAMIONS ==========================================================

    # Vérifie si le graph en orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    print("\n========================== EULER GRAPH ==========================")

    new_edges = make_directed_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, new_edges, True)
    path = findEulerianCircuit(nodes, nodes[0], adjList)

    # Pour x vehicules
    print("\n========================== PLUSIEURS VEHICULES ==========================")
    pretty_print.main_print(path, g, nbVehicules, essenceCost)

    # ============================================== TROTTOIRS =========================================================
    if walk:
        # Pour les trottoirs
        print("\n========================== TROTTOIRS et AUTANT DE VEHICULES ==========================")

        g = ox.graph_from_place(name_place, network_type='walk')

        nodes = list(g.nodes)
        edges = list(g.edges)

        new_edges = make_directed_eulerian(nodes, edges)
        adjList = helper.makeAdjList(nodes, new_edges, True)
        path = findEulerianCircuit(nodes, nodes[0], adjList)

        pretty_print.main_print(path, g, nbVehicules, essenceCost, False)


if __name__ == "__main__":
    # Vous pouvez modifier ici la ville à utiliser.
    g = ox.graph_from_place('Montréal, CANADA', network_type='drive')

    nodes = list(g.nodes)
    edges = list(g.edges)

    # =============================================== DRONE ============================================================

    # Vérifie si le graph en non orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    drone_edges = make_undirected_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, drone_edges, False)
    drone_path = findEulerianCircuit(nodes, nodes[0], adjList)

    print("\n========================== DRONE ==========================")
    pretty_print.drone_print(drone_path, g)
    print("Création d'un fichier 'drone.txt' avec le chemin du drone.")
    drone_path = None
    drone_edges = None

    # =============================================== CAMIONS ==========================================================

    # Vérifie si le graph en orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    print("\n========================== EULER GRAPH ==========================")

    new_edges = make_directed_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, new_edges, True)
    path = findEulerianCircuit(nodes, nodes[0], adjList)

    print("\n========================== UN VEHICULE ==========================")
    pretty_print.main_print(path.copy(), g, 1, 2.36)

    # Pour x vehicules
    print("\n========================== PLUSIEURS VEHICULES ==========================")
    x = 12
    pretty_print.main_print(path, g, x, 2.36)

    # ============================================== TROTTOIRS =========================================================

    # Pour les trottoirs
    print("\n========================== TROTTOIRS et AUTANT DE VEHICULES ==========================")

    g = ox.graph_from_place('Montréal, CANADA', network_type='walk')

    nodes = list(g.nodes)
    edges = list(g.edges)

    new_edges = make_directed_eulerian(nodes, edges)
    adjList = helper.makeAdjList(nodes, new_edges, True)
    path = findEulerianCircuit(nodes, nodes[0], adjList)

    pretty_print.main_print(path, g, x, 2.36, False)